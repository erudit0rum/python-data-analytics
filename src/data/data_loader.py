#this is based on code from several of the resources listed in the README
import pandas
import numpy
from sklearn import datasets
from pandas import read_csv
from pandas import concat
import data

def load_data_set(set, train=False):
    """
    in:
        set name,
        boolean indicating whether you would only like a sample of the entire data set
    out:
        output of the getter function triggered by the set name
    the idea here is that this module abstracts away from particular data sources, right now i
    only have the classification data sets from sklearn but eventually I will hook up all kinds of data sources and
    possibly data cleaning / ingestion code (web scraping is also a possibility as long as I can do it in a way that
    won't get me sued)
    """
    if set == "iris":
        data_set = load_iris_data_set()
    elif set == "digits":
        data_set = load_digits_data_set()
    elif set == "wine":
        data_set = load_wine_data_set()
    elif set == "young_people":
        data_set = load_young_people_data_set()
    else:
        raise Exception('You must pass a set name to load_data_set')

    if train:
        data_set = include_training_set(data_set)
    
    return data_set

def include_training_set(data_frame):
    #here we first pull a sample from the dataset
    train = data_frame.sample(frac=0.1)
    #then we pull the index from the training data (it retains the indexing of the original frame)
    #and drop all those records from the original data_frame and return that as the test set
    test = data_frame.drop(train.index, axis=0, inplace=False)
    return {
        'train': train,
        'test': test
    }

def load_iris_data_set():
    iris = datasets.load_iris()
    #the commas inside slice indexing is advanced indexing, see the article in the README
    iris_data_frame=pandas.DataFrame({
        'spl len':iris.data[:,0],
        'spl wd':iris.data[:,1],
        'ptl len':iris.data[:,2],
        'ptl wd':iris.data[:,3],
        'label':iris.target
    })
    return iris_data_frame

def load_digits_data_set():
    digits = datasets.load_digits()
    digits_data_frame=pandas.DataFrame({
        'first_col':digits.data[:,0],
        'second_col':digits.data[:,1],
        'third_col':digits.data[:,2],
        'fourth_col':digits.data[:,3],
        'fifth_col':digits.data[:,4],
        'sixth_col':digits.data[:,5],
        'seventh_col':digits.data[:,6],
        'eighth_col':digits.data[:,7],
        'ninth_col':digits.data[:,8],
        'label':digits.target
    })
    return digits_data_frame

def load_wine_data_set():
    wine = datasets.load_wine()
    wine_data_frame=pandas.DataFrame({
        'alchohol':wine.data[:,0],
        'malic_acid':wine.data[:,1],
        'ash':wine.data[:,2],
        'magnesium':wine.data[:,4],
        'flavanoids':wine.data[:,6],
        'phenols':wine.data[:,5],
        'label':wine.target
    })
    return wine_data_frame

def load_young_people_data_set():
    young_people = read_csv("src/data/young_people_responses.csv", delimiter=',')
    young_people.rename(columns={'Mathematics':'label'}, inplace=True)
    young_people = young_people.drop('Weight', 1)
    young_people = young_people.drop('Height', 1)
    young_people = young_people.drop('Age', 1)
    return young_people