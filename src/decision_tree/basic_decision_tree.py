#this is based on code from several of the resources listed in the README
#also heavily based on the stuff in data science from scratch
import pandas
import numpy
import sklearn
from sklearn import datasets
from data import data_loader
import data
import math
import pprint

def get_entropy(probabilities):
    """
    in: array of floats representing the probabilities of all the various labels
    out: float representing the entropy score
    """
    ntrpy = 0.0
    for probability in probabilities:
        if probability:
            ntrpy += math.log(probability, 2) * (-probability)
    return ntrpy

def get_class_probabilities(labels):
    """
    in: pandas series of labels
    out: array of floats representing the probabilities of all the various labels
    """

    #first we see how many of each label there are
    count_object = {}
    for label in labels.values:
        label = str(label)
        if not label in count_object:
            count_object[label] = 0
        count_object[label] += 1

    #then we use that information to calculate the chance any
    #of the records has of having a certain label
    total_count = len(labels)
    probabilities = []
    for val in count_object.values():
        probabilities.append(float(val / total_count))

    return probabilities

def get_data_entropy(data_frame):
    """
    in: pandas data_frame
    out: float representing the entropy score
    """
    labels = data_frame['label']
    probs = get_class_probabilities(labels)
    return get_entropy(probs)

def get_partition_entropy(data_frame, feature):
    """
    in:
    data_frame to partition
    feature to partition on
    out:
    entropy score
    """
    ntrpy = 0
    total_count = len(data_frame)
    for sub_frame in data_frame.groupby(feature):
        sub_frame = sub_frame[1]
        sub_count = len(sub_frame)
        weight = sub_count / total_count
        ntrpy += get_data_entropy(sub_frame) * weight
    return ntrpy

def same_label(data_frame):
    """
    in: dataframe
    out: boolean indicating whether or not every row of the dataframe has the same label
    """
    labels = data_frame['label']
    first_label = ''
    for label in labels.values:
        label = str(label)
        if first_label == '':
            first_label = label
            continue
        if first_label != label:
            return False
    return True

def get_first_label(data_frame):
    """
    in: data_frame
    out: string representing the label name of the first row
    """
    return str(data_frame['label'].values[0])

def get_most_common_label(data_frame):
    """
    in: data_frame
    out: string representing the name of the most common label
    """

    #first we count all the labels and record the amount of each in a dict
    label_count = {}
    labels = data_frame['label']
    for label in labels.values:
        label = str(label)
        if not label in label_count:
            label_count[label] = 0
        label_count[label] += 1

    #then we cycle through them and find the one with the highest count
    top_label = ['placeholder', 0]
    for key, value in label_count.items():
        if value > top_label[1]:
            top_label = [key, value]
    return str(top_label[0])

def remaining_attributes(data_frame):
    """
    in: data_frame
    out: boolean indicating whether or not there are any attributes remaining in the data_frame beside label
    """
    return data_frame.columns.size > 1

def get_attributes(data_frame):
    """
    in: data_frame
    out: pandas index of column names minus label
    """
    return data_frame.columns.drop(['label'])

def ID3_algorithm(data_frame):
    """
    in: data_frame
    out: decision tree (have to think more about what exactly
    that will be, is there a way to build decision trees in one of the
    libraries that I have been working with?)
    obviously this implements Ross Quinlans ID3 algorithm
    """
    if same_label(data_frame):
        return get_first_label(data_frame)
    elif not remaining_attributes(data_frame):
        return get_most_common_label(data_frame)
    else:
        prtn_atrbt = ''
        for atrbt in get_attributes(data_frame).values:
            atrbt = str(atrbt)
            entropy_score = get_partition_entropy(data_frame, atrbt)
            if prtn_atrbt == '':
                prtn_atrbt = [atrbt, entropy_score]
            if entropy_score < prtn_atrbt[1]:
                prtn_atrbt = [atrbt, entropy_score]

        prtn_atrbt = prtn_atrbt[0]
        out = {}
        out['branch_not_present'] = get_most_common_label(data_frame)
        for prtn in data_frame.groupby(prtn_atrbt):
            path = prtn[0]
            prpd_prtn = prtn[1].drop(prtn_atrbt, 1)
            path_return = ID3_algorithm(prpd_prtn)
            out[path] = path_return

        return {prtn_atrbt: out}

def classify_data_set(tree, data_frame):
    """
    in: representation of a tree, data set
    out: predictions on the data set label
    """
    total_len = len(data_frame)
    correct_pred_count = 0
    for row in data_frame.iterrows():
        row = row[1]
        pred_obj = classify_data_point(tree, row)
        if pred_obj['prediction'] == pred_obj['label']:
            correct_pred_count += 1

    return correct_pred_count / total_len

def classify_data_point(tree, point):
    """
    in: representation of a tree, data point as a pandas series
    out: predictions on the data set label
    """
    if type(tree) is str:
        outcome = {
            'prediction': str(tree),
            'label': str(point['label']),
        }
        return outcome
    
    branching_feature = list(tree.keys())[0]
    choice_node = tree[branching_feature]
    # whats going on here is my implementation of the algorithm
    # does not add branches to the decision tree for values
    # if those values are not present in the training data
    # in order to handle this I added some logic in the generation
    # phase to add a decision at every branch node for "branch_not_present"
    # which simply ends in the most likely label for that subset of the data
    # this logic takes the appropriate path if it is there, if it is not then
    # takes the "branch_not_present" branch
    if point[branching_feature] in choice_node:
        path = choice_node[point[branching_feature]]
    else:
        path = choice_node['branch_not_present']
    return classify_data_point(path, point)

def main():
    young_people_sample = data_loader.load_data_set('young_people', sample=True)
    young_people = data_loader.load_data_set('young_people')

    tree = ID3_algorithm(young_people_sample)
    pprint.pprint(tree)

    print(classify_data_set(tree, young_people))