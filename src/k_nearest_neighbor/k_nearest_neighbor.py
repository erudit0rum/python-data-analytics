#based on the stuff in introduction to statistical learning
import sys
import pandas
import numpy
import sklearn
from sklearn import datasets
from data import data_loader
import data
import math
from math import sqrt
import pprint
from collections import Counter
import pprint

def distance_between_points(point_a, point_b):
    """
    in: two points in n-dimensional space represented as pandas series
    (with only float or int value and no label)
    out: float representing distance between points
    this is a method to find the distance between two points in n dimensional space
    where n > 1, it seems to me that this problem can be solved recursively
    one interesting thing about this algorithm is that it technically involves taking the
    square root of numbers and then imediately squaring them again in the case of the right-hand
    recursive call
    https://hlab.stanford.edu/brian/euclidean_distance_in.html

    given the above this could probably be rewritten as a list comprehension
    """
    if len(point_a) == 1 and len(point_b) == 1:
        return abs(point_a[0] - point_b[0])
    else:
        return sqrt(
            distance_between_points(point_a[:1], point_b[:1])**2 +
            distance_between_points(point_a[1:], point_b[1:])**2)

def find_KNN(training_set, test_point, k):
    """
    in: training_set: training_data as a pandas dataframe, test_point: pandas vector representing the data point,
    point to find the
    sorted(tst)[::-1][:3]
    out: array of pandas series representing the k nearest neighbors

    this could stand to be rewritten for speed
    for instance, as we go we could add every distance to a dictionary, sort on key, and whenever there are four
    entries find the least and drop it, although not sure that would be faster
    """
    score_to_point = {}
    for idx, point in training_set.iterrows():
        #we have to remember to drop the labels before we pass the points into the
        #distance checker method
        score_to_point[
            distance_between_points(point.drop(labels=['label']), test_point.drop(labels=['label']))
        ] = point

    #the line below sorts score_to_point by key, reverses the order, slices off the top three results
    #then it cycles through those keys and returns the value stored at them in score_to_point
    return [score_to_point[label] for label in sorted(score_to_point)[:int(k)]]

def get_KNN_prediction(training_set, test_point, k):
    KNN = find_KNN(training_set, test_point, k)
    #the line below strips the label value off the rows included and feeds them all to a Counter,
    #then we call the counters most common method to return the most common result
    #then drill down to it
    return Counter([neighbor['label'] for neighbor in KNN]).most_common(1)[0][0]

def KNN(training_set, test_set, k):
    results_array = []
    total = len(test_set)
    error = 0
    for index, test_point in test_set.iterrows():
        result = {
            "prediction": get_KNN_prediction(training_set, test_point, k),
            "actual": test_point['label']
        }
        results_array.append(result)
        if result['prediction'] != result['actual']:
            error += 1
    return {
        'error %': error/total,
        'results': results_array
    }

def main():
    young_people = data_loader.load_data_set('iris', train=True)

    #print(KNN(young_people['train'], young_people['test'], 55)['error %'])
    pprint.pprint(KNN(young_people['train'], young_people['test'], 3))