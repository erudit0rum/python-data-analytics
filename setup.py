from setuptools import setup
import startup

requires = [
    "pytest==5.2.2",
    "pytest-runner==5.2",
    "pandas==0.25.3",
    "numpy==1.16.2",
    "scikit-learn==0.21.3",
]

setup(
    name="python-data-analytics",
    version="0.0.0",
    author="Isaac Duarte",
    author_email="isaac.bn.duarte@gmail.com",
    license="MIT",
    packages=['data', 'decision_tree'],
    package_dir={'':'src'},
    test_suite='tests',
#    scripts=['startup.py'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    entry_points={"console_scripts": [
        "decision_tree_main = decision_tree.basic_decision_tree:main",
        "k_nearest_main = k_nearest_neighbor.k_nearest_neighbor:main"]},
)