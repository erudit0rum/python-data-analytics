# python-data-analytics

Collection of Data analytics projects in python

Purpose is to understand the underlying principles of data analytics algorithms
thus, many things will be hand written that could otherwise be handed off to sklearn
if this was a serious project and not a learning exercise

important commands:

python3 setup.py develop (this will create the script from the entry point in your /usr/lib/bin)
python3 setup.py test (this will collect the stuff from the tests directory in normal pytest fashion)

resources:

https://www.analyticsvidhya.com/blog/2015/09/build-predictive-model-10-minutes-python/

https://able.bio/SamDev14/how-to-structure-a-python-project--685o1o6

https://coderwall.com/p/lt2kew/python-creating-your-project-structure

https://justin.abrah.ms/python/setuppy_distutils_testing.html

https://setuptools.readthedocs.io/en/latest/index.html

https://stackoverflow.com/questions/22180315/how-to-make-pylint-a-part-of-setup-py-test-process

https://pypi.org/project/pytest-runner/

https://www.datacamp.com/community/tutorials/random-forests-classifier-python

#advanced indexing with numpy arrays:
https://stackoverflow.com/questions/17370820/python-slice-notation-with-comma-list

# example python project

https://github.com/home-assistant

data source:

https://www.kaggle.com/miroslavsabo/young-people-survey

any data sets not listed here are built in sklearn sets

# TO DO:
* Random Tree algorithm implemented
* Set up auto dev ops to automatically deploy to my ec2 instance
    https://medium.com/@lucabecchetti/autodeploy-from-gitlab-to-multiple-aws-ec2-instances-a43448727c5a
* Implement K-Nearest neighbors